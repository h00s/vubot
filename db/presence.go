package db

import (
	"errors"
	"time"

	"github.com/boltdb/bolt"
	"github.com/bwmarrin/discordgo"
)

// GetLastSeen returns last seen data for desired user
func (db *Database) GetLastSeen(userID string) ([]byte, error) {
	var seen []byte
	err := db.Conn.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketPresences))
		seen = bucket.Get([]byte(userID))
		return nil
	})
	if err != nil {
		return nil, errors.New("Error while getting seen data for this user")
	}
	return seen, nil
}

// UpdatePresence update presence of current user in DB
func (db *Database) UpdatePresence(p *discordgo.Presence) error {
	currentTime := []byte("0")
	if p.Status == discordgo.StatusInvisible || p.Status == discordgo.StatusOffline {
		currentTime, _ = time.Now().MarshalText()
	}
	err := db.Conn.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(BucketPresences))
		return bucket.Put([]byte(p.User.ID), currentTime)
	})
	return err
}
