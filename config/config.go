package config

import (
	"github.com/BurntSushi/toml"
)

// Configuration struct have all configuration options
type Configuration struct {
	Token            string
	DatabaseFilename string
	Log              string
}

// Load loads configuration from path
func Load(path string) (*Configuration, error) {
	c := new(Configuration)

	if _, err := toml.DecodeFile(path, c); err != nil {
		return c, err
	}

	return c, nil
}
