package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"vubot/bot"
	"vubot/config"
	"vubot/db"
	"vubot/logger"
)

func main() {
	c, err := config.Load("vubot.toml")
	if err != nil {
		fmt.Printf("Fail to read configuration: %v", err)
		os.Exit(1)
	}

	l, err := logger.New(c.Log)
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()

	db, err := db.Connect(c.DatabaseFilename)
	if err != nil {
		l.Fatal("Unable to connect to DB")
	}
	defer db.Close()

	b, err := bot.New(c, db, l)
	if err != nil {
		fmt.Println("Unable to start bot: " + err.Error())
		l.Fatal("Unable to start bot: " + err.Error())
	}

	fmt.Println("VUBot is now running. Press CTRL-C to exit.")
	l.Info("VUBot is now running.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	b.Close()
	l.Info("VUBot stopped.")
}
