package bot

import (
	"errors"
	"strings"

	"vubot/config"
	"vubot/db"
	"vubot/logger"

	"github.com/bwmarrin/discordgo"
)

// Bot handles discord messages and presence notifications
type Bot struct {
	Config  *config.Configuration
	DB      *db.Database
	Logger  *logger.Logger
	Session *discordgo.Session
	Users   map[string]string
}

// New creates new bot
func New(c *config.Configuration, d *db.Database, l *logger.Logger) (*Bot, error) {
	var err error
	b := new(Bot)
	b.Config = c
	b.Logger = l
	b.DB = d

	b.Session, err = discordgo.New("Bot " + b.Config.Token)
	if err != nil {
		return b, err
	}

	b.Session.AddHandler(b.messageCreated)
	b.Session.AddHandler(b.presenceUpdated)

	err = b.Session.Open()
	if err != nil {
		return b, err
	}

	b.Users = make(map[string]string)
	b.UpdateUsers()
	b.updatePresences()

	return b, nil
}

func (b *Bot) messageCreated(_ *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == b.Session.State.User.ID {
		return
	}

	if m.Content == "?hello" {
		b.hello(m)
	} else if strings.HasPrefix(m.Content, "?seen ") {
		b.seen(m)
	}
}

func (b *Bot) presenceUpdated(_ *discordgo.Session, m *discordgo.PresenceUpdate) {
	b.updatePresence(m)
}

// Close closes opened discord session
func (b *Bot) Close() {
	b.Session.Close()
}

// UpdateUsers get Users list and stores it in map
func (b *Bot) UpdateUsers() {
	members, _ := b.Session.GuildMembers(b.Session.State.Guilds[0].ID, "", 1000)
	for _, member := range members {
		b.Users[member.User.ID] = strings.ToLower(member.User.Username)
	}
}

// GetUserID returns ID for given username
func (b *Bot) GetUserID(username string) (string, error) {
	username = strings.ToLower(username)
	var UserID string
	for ID, currentUsername := range b.Users {
		if currentUsername == username {
			UserID = ID
			break
		}
	}

	// TODO: if user not found, update map!
	if UserID == "" {
		return UserID, errors.New("User does not exist")
	}
	return UserID, nil
}
